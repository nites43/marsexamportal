import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KhaltiPaymentComponent } from './khalti-payment.component';

describe('KhaltiPaymentComponent', () => {
  let component: KhaltiPaymentComponent;
  let fixture: ComponentFixture<KhaltiPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KhaltiPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KhaltiPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
