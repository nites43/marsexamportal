import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebaseService'
import { Question, AnswerResponse } from '../models/QuestionModel';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.css']
})
export class ExamComponent implements OnInit {
  public QuestionList: Array<Question>;
  public AnswerList :AnswerResponse [] = [];
  constructor(
    public dataservice: FirebaseService
  ) { }

  ngOnInit() {
    console.log("started");
    this.GetQuestions();

  }
  public GetQuestions() {
    var fireRequest = this.dataservice.getAllQuestions().subscribe((fireResponse) => {
      this.QuestionList = fireResponse.map(e => {
        return {
          A: e.payload.doc.data()['A'],
          B: e.payload.doc.data()['B'],
          C: e.payload.doc.data()['C'],
          D: e.payload.doc.data()['D'],
          Question: e.payload.doc.data()['QUESTIONS'],
          Correct: e.payload.doc.data()['Correct'],
          Id: e.payload.doc.data()['SN']
        }
      });
      console.log(this.QuestionList);
    });
  }

  public GetSelectedAnswer(selectedValue, correct, questionId,question) 
  {
    debugger;
    var objanswerResponse  : AnswerResponse = new AnswerResponse(selectedValue,correct,questionId,question);
    if(this.AnswerList.length === 0 ) {
      this.AnswerList.push(objanswerResponse);
    }
    else {
      this.AnswerList.map((objanswerResponse=>{
        for (var i = this.AnswerList.length - 1; i >= 0; i--){
          console.log(i);
          if(this.AnswerList[i].QuestionId === objanswerResponse.QuestionId) {
            console.log("match");
          }
        }
      })
      )
    }
    console.log(this.AnswerList);
  }
}
