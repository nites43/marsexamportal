import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KhaltiPaymentComponent } from './khalti-payment/khalti-payment.component';
import { ExamComponent } from './exam/exam.component';

const routes: Routes = [
  { path: 'checkout', component:KhaltiPaymentComponent},
  { path: 'taketest', component:ExamComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
